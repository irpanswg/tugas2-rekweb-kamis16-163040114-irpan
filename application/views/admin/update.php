<h3 class="text-center">Form pengubahan data produk</h3>
<form action="<?php echo site_url('page/edit') ?>" method="post" enctype="multipart/form-data">
<?php foreach($prk as $prk): ?>
  <div class="form-group">
    <!-- <label for="exampleInputEmail1">ID Produk</label> -->
    <input type="hidden" class="form-control-filetrol" id="exampleInputId"  placeholder="Masukan ID Produk" name="id" value="<?php echo $prk->id_produk ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Nama Produk</label>
    <input type="text" class="form-control" id="exampleInputNama"  placeholder="Masukan Nama Produk" name="nama_produk" value="<?php echo $prk->nama_produk ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Deskripsi</label>
    <input style="width: 100%; height: 100px;" type="text" class="form-control" id="exampleInputDeskripsi" placeholder="Masukan Deskripsi" name="deskripsi" value="<?php echo $prk->deskripsi ?>">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1">Harga</label>
    <input type="number" class="form-control" id="exampleInputHarga" placeholder="Masukan Harga" name="harga" value="<?php echo $prk->harga ?>">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1">Jumlah Barang</label>
    <input type="number" class="form-control" id="exampleInputStok" placeholder="Masukan jumlah stok barang" name="stok" value="<?php echo $prk->stok ?>">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Gambar</label>
    <input type="hidden" name="image" value="<?php echo $prk->gambar ?>">
    <input type="file" class="form-control-file" id="exampleFormGambar" name="gambar">
    <small>Ukuran size 5MB</small>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label" for="exampleCheck1">Kategori </label>&nbsp;
    <input type="hidden" name="ktgr" value="<?php echo $prk->kategori ?>">
    <select name="kategori">
  <option value="0" selected> Pilih </option>
  <option value="1">Remote Control</option>
  <option value="2">Boneka</option>
  <option value="3">Action Figure</option>
</select>
  </div>
  
  <button type="submit" class="btn btn-success" name="submit">Ubah</button>
<?php endforeach ?>
</form>